import IdGenerator from '@/utils/IdGenerator'
import { EncryptedStream } from 'extension-streams'
import * as MsgTypes from './messages/types'
import * as EventNames from '@/messages/event'
import Bytomdapp from './dapp'
import { provider } from 'bytom'

/***
 * This is the javascript which gets injected into
 * the application and facilitates communication between
 * bytom chrome extension and the web application.
 */
class Inject {
  constructor() {
    // Injecting an encrypted stream into the web application.
    const stream = new EncryptedStream(EventNames.INJECT, IdGenerator.text(64))

    // Waiting for bytomExtension to push itself onto the application
    stream.listenWith(msg => {
      console.log('inject.stream.listen:', msg)
      if (
        msg &&
        msg.hasOwnProperty('type') &&
        msg.type === MsgTypes.PUSH_BYTOM
      ) {
        // window.bytom only for ofmf business get vpAddress, other mov-web business use bytom2
        window.bytom = new Bytomdapp(stream, msg.payload)
        new provider.ByoneProvider({ 
          logger: true, 
          stream, 
          net: msg.payload.net, 
          chain: msg.payload.chain,
          account: msg.payload.defaultAccount && {
            address: msg.payload.defaultAccount.address,
            xpub: msg.payload.defaultAccount.rootXPub
          }
        })
      }

      if (
        msg &&
        msg.hasOwnProperty('type') &&
        msg.type === MsgTypes.UPDATE_BYTOM
      ) {
        const array = msg.payload;
        for(let p of array){
          window.bytom[p.type] = p.value
          switch (p.type){
            case 'default_account':{
              window.bytom.emit(MsgTypes.ACCOUNT_CHANGED, [p.value])
              window.bytom2.setAccount(p.value && {
                address: p.value.address,
                xpub: p.value.rootXPub
              })
              break
            }
            case 'net':{
              window.bytom.emit(MsgTypes.NETWORK_CHANGED, p.value)
              window.bytom2.setNet(p.value)
              break
            }
            case 'chain':{
              window.bytom.emit(MsgTypes.NET_TYPE_CHANGED, p.value)
              window.bytom2.setChain(p.value)
              break
            }
          }
        }
      }

      if (
        msg && 
        msg.hasOwnProperty('type') && 
        msg.type === MsgTypes.ENABLE
      ) {
        window.bytom.defaultAccount = window.bytom.default_account = {
          address: msg.payload.address,
          addresses: msg.payload.addresses,
          xpub: msg.payload.xpub
        }
      }

      if (
        msg && 
        msg.hasOwnProperty('type') && 
        msg.type === MsgTypes.DISABLE
      ) {
        console.log('disabledisable')
        window.bytom.defaultAccount = window.bytom.default_account = null
      }
    })

    // Syncing the streams between the extension and the web application
    stream.sync(EventNames.BYTOM, stream.key)
  }
}

new Inject()
